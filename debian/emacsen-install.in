#! /bin/sh
# /usr/lib/emacsen-common/packages/install/@PACKAGE@
set -e

FLAVOR=$1
PACKAGE=@PACKAGE@
PKGSNAME=w3m
PKGSNAME2=w3m-el

ELCDIR=/usr/share/$FLAVOR/site-lisp/$PKGSNAME
ELDIR=/usr/share/$PKGSNAME2
ELCSTAMP=$ELCDIR/compile-stamp

case $FLAVOR in
    *xemacs*|emacs2[0-5]|emacs1?|mule2)
    exit 0
    ;;
esac

if [ -f "$ELCSTAMP" ]; then
    if [ -z "`find "$ELDIR/$PKGSNAME.el" -newer "$ELCSTAMP"`" ]; then
	echo "install/$PACKAGE: already byte-compiled for $FLAVOR, skipped"
	exit 0
    fi
    rm -f "$ELCSTAMP"
fi

LOG=`mktemp -t elc.XXXXXXXXXXXX`
chmod 644 "$LOG"
echo "install/$PACKAGE: byte-compiling for $FLAVOR, logged in $LOG"

cd "$ELDIR"
LINKS=`echo *.el`
cd shimbun
LINKS2=`echo *.el`
if [ ! -d "$ELCDIR" ]; then
    mkdir "$ELCDIR"
    chmod 755 "$ELCDIR"
fi
if [ ! -d "$ELCDIR/shimbun" ]; then
    mkdir "$ELCDIR/shimbun"
    chmod 755 "$ELCDIR/shimbun"
fi

cd "$ELCDIR"
TOELDIR=../../../$PKGSNAME2
TOELDIR2=../$TOELDIR/shimbun
rm -f *.elc __myinit.el
for f in $LINKS; do
    ln -sf "$TOELDIR/$f" .
done
FILES=`/bin/ls -1 *.el | egrep -v '^(w3mhack.el|w3m-setup.el|w3m-load.el|mew-w3m.el|mime-w3m.el|octet.el|w3m-ucs.el)'`

cat > __myinit.el << EOF
(setq load-path (cons "." load-path))
(or (fboundp 'declare-function) (defmacro declare-function (&rest args)))
(setq byte-compile-warnings nil)
EOF
if [ -f "/usr/share/$FLAVOR/site-lisp/apel/poe.elc" ]; then
    cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/apel" load-path))
EOF
    if [ -f "/usr/share/$FLAVOR/site-lisp/flim/mime.elc" ]; then
	if [ -f octet.el ]; then
	    FILES="$FILES octet.el"
	fi
	cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/flim" load-path))
EOF
	if [ -f "/usr/share/$FLAVOR/site-lisp/semi/mime-view.elc" ]; then
	    if [ -f mime-w3m.el ]; then
		FILES="$FILES mime-w3m.el"
	    fi
	    cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/semi" load-path))
EOF
	fi
    fi
fi
if [ -f mew-w3m.el ] && [ -f "/usr/share/$FLAVOR/site-lisp/mew/mew.elc" ]; then
    FILES="$FILES mew-w3m.el"
    cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/mew" load-path))
EOF
fi
FLAGS="-q -no-site-file -batch -l __myinit.el -l w3mhack.el . -f w3mhack-generate-load-file"
echo "$FLAVOR" $FLAGS >> "$LOG"
"$FLAVOR" $FLAGS >> "$LOG" 2>&1

FLAGS="-q -no-site-file -batch -l __myinit.el -f batch-byte-compile"
echo "$FLAVOR" $FLAGS $FILES >> "$LOG"
"$FLAVOR" $FLAGS $FILES >> "$LOG" 2>&1
chmod 644 *.elc
rm -f __myinit.el*

if [ -d shimbun ] && [ -f "/usr/share/$FLAVOR/site-lisp/flim/mime.elc" ]; then
    cd shimbun
    rm -f *.elc __myinit.el
    for f in $LINKS2; do
	ln -sf "$TOELDIR2/$f" .
    done
    FILES=`/bin/ls -1 *.el | egrep -v '^(mew-shimbun.el|nnshimbun.el)'`

    cat > __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/apel" load-path))
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/flim" load-path))
(setq load-path (cons ".." load-path))
(setq load-path (cons "." load-path))
(or (fboundp 'declare-function) (defmacro declare-function (&rest args)))
(setq byte-compile-warnings nil)
EOF
    if [ -f mew-shimbun.el ] && [ -f "/usr/share/$FLAVOR/site-lisp/mew/mew.elc" ]; then
	FILES="$FILES mew-shimbun.el"
	cat >> __myinit.el << EOF
(setq load-path (cons "/usr/share/$FLAVOR/site-lisp/mew" load-path))
EOF
    fi

    FLAGS="-q -no-site-file -batch -l __myinit.el -f batch-byte-compile"
    echo "$FLAVOR" $FLAGS $FILES >> "$LOG"
    "$FLAVOR" $FLAGS $FILES >> "$LOG" 2>&1
    chmod 644 *.elc
    rm -f __myinit.el*
    cd ..
fi

if [ -f /usr/share/emacsen-common/debian-startup.el ] && \
   [ ! -f "/etc/$FLAVOR/site-start.d/50$PACKAGE.el" ] && \
   [ -f "/etc/emacs/site-start.d/50$PACKAGE.el" ] && \
   [ -d "/etc/$FLAVOR/site-start.d" ]; then
    ln -sf "../../emacs/site-start.d/50$PACKAGE.el" "/etc/$FLAVOR/site-start.d/50$PACKAGE.el"
fi

echo "install/$PACKAGE: deleting $LOG"
rm -f "$LOG"

echo "install/$PACKAGE: byte-compiling for reverse dependency"
pkgs=
for p in wl:wl wl-beta:wl; do
    pkg=`echo $p | cut -d : -f 1`
    sname=`echo $p | cut -d : -f 2`
    dir=/usr/share/$FLAVOR/site-lisp/$sname
    if [ -n "$sname" ] && [ -d "$dir" ] && [ -f "/usr/lib/emacsen-common/packages/install/$pkg" ]; then
	rm -f "$dir"/*.elc "$dir"/*/*.elc "$dir"/*-stamp
	pkgs="$pkg $pkgs"
    fi
done
for pkg in $pkgs; do
    "/usr/lib/emacsen-common/packages/install/$pkg" "$FLAVOR"
done

touch "$ELCSTAMP"
exit 0
